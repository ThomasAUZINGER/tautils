# -*- coding: utf-8 -*-
"""Glass Cannon Podcast Downloader.

This module allows the creation of offline playlists of all public episodes of
the various channels (e.g., Giantslayer, Cannon Fodder, ...) of the Glass
Cannon Podcast (https://glasscannonpodcast.com/). The program
performs the following actions:

- Discover all available episodes of the known channels.
- Download all episodes into appropriate subfolders.
- Create an M3U playlist with all episodes chronologically ordered according
  to publishing date.

Examples
--------
The standard way of calling this program is::

    $ python gcp_download D:\\glass-cannon

where ``D:\\glass-cannon`` is an exemplary download folder. Note that this
will download dozens of gigabytes of media files.

In order to check if the program is generating the correct output, the
argument ``--playlist`` can be used, which creates the subfolders and the
playlist file but does *not* download the media files. Instead, the program
reports which files would be downloaded::

    $ python gcp_download D:\\glass-cannon --playlist

The supported command line arguments can be listed using::

    $ python gcp_download --help

"""

import argparse
import os
import errno
import urllib
import shutil
from datetime import datetime
from collections import namedtuple
import xmltodict
from sortedcontainers import SortedDict

DESCRIPTION = ('Create a chronological playlist of the Glass Cannon Podcast'
               ' and download the associated media files.')
EPILOG = ('Developed by Thomas AUZINGER <thomas@auzinger.name> and licensed'
          ' under GPL 3.0.')

Channel = namedtuple('Channel', ['rss_url', 'folder_name'])
Channel.__doc__ += """: A channel of the podcast."""
Channel.rss_url.__doc__ = """The URL of the XML file of the channel's RSS
feed."""
Channel.folder_name.__doc__ = """The name of the subfolder to which the media
files of the channel should be downloaded."""


def get_channels():
    """Get the known channels of the podcast.

    Returns
    -------
    channels : list
        A list of channels, where each element has the members ``rss_url`` and
        ``folder_name``, which give the URL of each channel's RSS feed and
        the name of the subfolder to which the channel's media files should be
        downloaded.

    Notes
    -----
    This function allows customization of the module's behavior:

    - If the URL of the podcast's RSS feeds changes, the ``base_url`` variable
      has to be adapted.
    - If the URL of a single channel's RSS feed changes, the corresponding
      ``rss_url`` needs to be updated.
    - If new channels are created, a corresponding ``Channel`` tuple needs to
      be created with the appropriate information.
    - If channels should be ignored in the creation of the playlist, the
      corresponding ``Channel`` needs to be removed from the returned list.
    - If the media files of a channel should be downloaded to a different
      subfolder, the corresponding ``folder_name`` needs to be altered.
    """
    base_url = 'https://www.blubrry.com/feeds/'

    giantslayer_channel = Channel(base_url + 'the_glass_cannon.xml',
                                  'giantslayer')
    cannon_fodder_channel = Channel(base_url + 'cannonfodder.xml',
                                    'cannon-fodder')
    androis_and_aliens_channel = Channel(base_url + 'androidsandaliens.xml',
                                         'androids-and-aliens')
    return [giantslayer_channel,
            cannon_fodder_channel,
            androis_and_aliens_channel]


def parse_arguments():
    """Parse the command line arguments.

    Returns
    -------
    args : dict
        A dictionary holding the supplied command line arguments.

    Notes
    -----
    Run ``$ python gcp_download --help`` for a listing of all supported command
    line arguments.
    """
    parser = argparse.ArgumentParser(description=DESCRIPTION, epilog=EPILOG)
    parser.add_argument(
        'target', help=(
            'the target folder where the playlist and the media'
            ' files are stored.'), metavar='T')
    parser.add_argument(
        '-p',
        '--playlist',
        help=(
            'create playlist and subfolders but only report which media'
            ' files would be downloaded.'),
        action='store_true')
    parser.add_argument('-V',
                        '--version',
                        action='version',
                        version='%(prog)s 1.1')
    return parser.parse_args()


def collect_rss_items(url, folder):
    """Collect items of an RSS feed.

    Given a ``url`` of an RSS feed and a target ``folder``, this function
    discovers the items of the feed and stores both their source links and
    the paths to their download location in the target ``folder``.

    Parameters
    ----------
    url : str
        A URL of an XML file of an RSS feed.

    folder: str
        A path to a folder to which the items of the RSS will be downloaded to.

    Returns
    -------
    result : dict
        A dictionary, whose values have members ``source_link`` and
        ``target_path``, which correspond to the source and target of a
        download operation.  The dictionary uses the publishing date of each
        item as key.

    Notes
    -----
    This function assumes that the XML file of the RSS feed has the following
    structures::

        <rss>
          ...
          <channel>
            ...
            <item>
              ...
              <pubDate>...</pubDate>
              <enclosure url="..." ... />
            </item>
            <item>
              ...
            </item>
          </channel>
        </rss>

    See [1]_ for details on the download functionality.


    References
    ----------
    .. [1] Oleh Prypin, Stack Overflow,
       https://stackoverflow.com/a/7244263/1835723.
    """
    print('Downloading {} ...'.format(url))
    response = urllib.request.urlopen(url)
    data = response.read()
    xml = data.decode(response.info().get_param('charset', 'utf-8'))
    print('Downloading {} finished.'.format(url))

    LinkedFile = namedtuple('LinkedFile', ['source_link', 'target_path'])
    result = {}

    print('Parsing XML ...')
    doc = xmltodict.parse(xml)  # Convert XML structure to dictionaries.
    items = doc['rss']['channel']['item']  # Get a list of all items.
    for item in items:
        link = item['enclosure']['@url']  # Get download link.
        name = link.split('?')[0]
        path = os.path.join(folder, os.path.basename(name))
        datetime_string = item['pubDate']  # Get publishing date.
        time = datetime.strptime(datetime_string, '%a, %d %b %Y %H:%M:%S %z')
        result[time] = LinkedFile(source_link=link,
                                  target_path=path)
    print('Parsing XML finished.')
    return result


def download_gcp(channels, download_folder, only_playlist=False):
    """Download the media files and generate playlist.

    Given a list of ``channels`` and a ``download_folder``, this function
    downloads the media files of all channels to the folder and creates a
    chronological playlist of these files according to their publishing date.

    Parameters
    ----------
    channels : list
      A list of ``Channel`` objects.

    download_folder : str
      A path to the folder where the playlist will be created and to which the
      media files are downloaded.

    only_playlist : bool, optional
      If ``True``, the download of the media files is skipped.

    Notes
    -----
    The filename of the playlist is ``all.m3u``.

    Existing media files are *not* downloaded again and skipped.

    See [1]_ for details on the download functionality.

    See Also
    --------
    Channel : Channel description.

    get_channels : Get relevant channels.
    """

    # Media files are sorted according to publishing date.
    all_items = SortedDict()
    for channel in channels:
        url, folder = channel.rss_url, channel.folder_name
        all_items.update(collect_rss_items(url, folder))

    playlist_filename = 'all.m3u'
    playlist_path = os.path.join(download_folder, playlist_filename)
    os.makedirs(os.path.dirname(playlist_path), exist_ok=True)
    # Generate playlist.
    with open(playlist_path, 'w') as playlist:
        print('Generating playlist ...')
        for item in all_items.values():
            playlist.write(item.target_path + '\n')  # Add file to playlist.
        print('Generating playlist finished.')

    # Download media files.
    for item in all_items.values():
        full_target_path = os.path.join(download_folder, item.target_path)
        os.makedirs(os.path.dirname(full_target_path), exist_ok=True)
        if os.path.exists(full_target_path):
            # TODO: Ensure correct file size of existing file.
            print('File at {} already exists.'.format(full_target_path))
            continue  # Avoid redownloading existing file.
        if not only_playlist:
            print('Downloading {} ...'.format(item.target_path))
            # See https://stackoverflow.com/a/7244263/1835723.
            with urllib.request.urlopen(item.source_link) as response, \
                    open(full_target_path, 'wb') as target_file:
                shutil.copyfileobj(response, target_file)
            print('Downloading {} finished.'.format(item.target_path))
        else:
            print('Would download from {} to {}.'.format(item.source_link,
                                                         full_target_path))


if __name__ == '__main__':
    ARGS = parse_arguments()
    os.makedirs(ARGS.target, exist_ok=True)
    download_gcp(get_channels(), ARGS.target, ARGS.playlist)
    print('FINISHED')
