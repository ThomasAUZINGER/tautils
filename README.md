# TAUtils

[![pipeline status](https://gitlab.com/ThomasAUZINGER/tautils/badges/master/pipeline.svg)](https://gitlab.com/ThomasAUZINGER/tautils/commits/master)

An assortment of utility programs.
So far, it contains:
- The **Glass Cannon Podcast Downloader** to create a chronological playlists of all channels.<br>
    See [below](#glass-cannon-podcast-downloader) for details.

## Progams

### Glass Cannon Podcast Downloader

This Python script allows the creation of offline playlists of the Glass Cannon Podcast (see <https://glasscannonpodcast.com/>).
The playlist is in chronological order according to publication date and all public channels (e.g., Giantslayer, Cannon Fodder, etc.) are included.

To run it, several prerequisites have to be met:
1. Python3 needs to be installed (see <https://docs.python-guide.org/starting/installation/>)
2. The program needs to be downloaded either using the download button above or cloning the whole repository using Git.
3. The folder `source/python/` needs to be opened in the command prompt (for Windows) or the terminal (for Mac or Linux).

In the command prompt (resp. terminal), run the following commands to install all dependencies:
1. `pip3 install --upgrade pip`
2. `pip3 install -r requirements.txt`

To run the actual program, see the **online documentation** at <https://thomasauzinger.gitlab.io/tautils>.

## Authors

- Thomas AUZINGER - [webpage](http://auzinger.name)

## License

This project is licensed under the GNU GPLv3 license - see the [LICENSE][license] file for details.

[license]: LICENSE
